#!/bin/bash

# Prepare log files and start outputting logs to stdout
mkdir -p /code/logs
cd /code

export DJANGO_SETTINGS_MODULE=talpoint.settings

exec gunicorn talpoint.wsgi:application \
    --name talpoint_interview \
    --bind 0.0.0.0:8000 \
    --workers 4 \
    --log-level=debug
"$@"