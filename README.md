# Django Rest Framework Interview
Django Rest Framework application that will be used by the candidates interviewing at [Talpoint][4].

## Dependencies
This project relies on Docker, mainly:
  - docker-compose
  
Full list of dependencies can be found in [requirements.txt][1].

## Requirements:
  - Candidates expected to clone this repo and implement required features based on inputs provided by the interviewer
  - Be ready with a cloned version of this repo well before the start of interview

## Nice to Have:
It will be an advantage for candidates to demonstrate the following:
  - Proper usage of Http Methods and REST practices
  - Demonstrate their understanding of Python, [Django][2] and [Django Rest Framework][3] best practices.

[1]: requirements.txt
[2]: https://www.djangoproject.com/
[3]: https://www.django-rest-framework.org/
[4]: https://talpoint.in